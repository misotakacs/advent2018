package main

import (
  "fmt"
  "os"
  "bufio"
//  "regexp"
  "strconv"
  "strings"
)

func readLines() []string {
    scanner := bufio.NewScanner(os.Stdin)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        panic(err)
    }

    return lines
}

func atoi(s string) int {
    a, err := strconv.Atoi(s)
    if err != nil {
        panic(err)
    }
    return a
}

type clay struct {
    x1 int
    y1 int
    x2 int
    y2 int
}

func parseExpansion(e string) (int, int) {
    e = e[2:]
    if strings.Contains(e, "..") {
        p := strings.Split(e, "..")
        return atoi(p[0]), atoi(p[1])
    }
    return atoi(e), atoi(e)
}

type coord struct {
    x int
    y int
}

type el int

const (
    EMPTY el = iota
    CLAY el = iota
    WET el = iota
    WATER el = iota
    SPRING el = iota
)

func render(min, max coord, space map[coord]el, path map[coord]bool, tags map[coord]int) {
    for j := min.y-1; j <= max.y+1; j++ {
        for i := min.x-1; i <= max.x+1; i++ {
            c := ". "
            sp := space[coord{i, j}]
            switch sp {
                case EMPTY:
                    if path[coord{i,j}] {
                        c = "| "
                    } else {
                        c = ". "
                    }
                case CLAY:
                    c = "# "
                case WATER:
                    c = "~ "
                case WET:
                    c = "| "
                case SPRING:
                    c = "+ "
            }
            if tags[coord{i,j}] > 0 {
                c = fmt.Sprintf("%02d", tags[coord{i,j}] % 100)
            }
            fmt.Printf("%s", c)
        }
        fmt.Printf("\n")
    }
}

type pos struct {
    c coord
    bounced bool
}

func down(c coord) coord {
    return coord{c.x, c.y+1}
}

func left(c coord) coord {
    return coord{c.x-1,c.y}
}

func right(c coord) coord {
    return coord{c.x+1,c.y}
}

func traceWater(min, max coord, space map[coord]el) {
    reach:= map[coord]bool{}
    it := 0
    reachlen := -1
    retained := map[coord]bool{}
    for len(reach) > reachlen {
        reachlen = len(reach)
        tags := map[coord]int{}
        it++
        ctag := 1
        next := []coord{coord{500,1}}
        pathy := map[int][]coord{}
        path := map[coord]bool{
            coord{500,1}: true,
        }
        maxy := 0
        bytag := map[int][]coord{}
        canceltag := map[int]bool{}
        for len(next) > 0 {
            c := next[len(next)-1]
            tag := tags[c]
            if tag == 0 {
                tag = ctag
                tags[c] = tag
            }
            bytag[tag] = append(bytag[tag], c)
            if c.y >= min.y && c.y <= max.y {
                reach[c] = true
            }
            if c.y > maxy {
                maxy = c.y
            }
            // Record path.
            pathy[c.y] = append(pathy[c.y], c)
            next = next[:len(next)-1]

            // Add next steps.
            if space[down(c)] == EMPTY {
                canceltag[tag] = true
                if !path[down(c)] && c.y+1 <= max.y {
                    // if we can go down from this branch, it's not still water. 
                    next = append(next, down(c))
                    path[down(c)] = true
                    ctag++
                    tags[down(c)] = ctag
                }
            } else {
                if !path[left(c)] && space[left(c)] == EMPTY {
                    next = append(next, left(c))
                    tags[left(c)] = tag
                    path[left(c)] = true
                }
                if !path[right(c)] && space[right(c)] == EMPTY {
                    next = append(next, right(c))
                    tags[right(c)] = tag
                    path[right(c)] = true
                }
            }
        }

        for t, cs := range bytag {
            if !canceltag[t] {
                for _, c := range cs {
                    space[c] = WATER
                    retained[c] = true
                }
            }
        }

/*
        if it >= 156 {
            render(min, max, space, reach, tags)
        }
*/
//        if maxy < max.y {
//            for _, c := range pathy[maxy] {
//                space[c] = WATER
//            }
//        }
//        render(min, max, space, path)
 /*       if maxy == max.y {
            break
        }
        */
    }

    render(min, max, space, reach, map[coord]int{})

    fmt.Printf("Reach: %d\n", len(reach))
    fmt.Printf("Retained: %d\n", len(retained))
}

func main() {
    cs := []*clay{}
    minx, miny := -1, -1
    maxx, maxy := -1, -1

    space := map[coord]el{}

    for _, l := range readLines() {
        p := strings.Split(l, ", ")
        x, y := p[0], p[1]
        if x[0] == 'y' {
            x, y = y, x
        }
        c := &clay{}
        c.x1, c.x2 = parseExpansion(x)
        c.y1, c.y2 = parseExpansion(y)


        //fmt.Printf("%v\n", c)

        cs = append(cs, c)
        
        if c.x1 < minx || minx == - 1{
            minx = c.x1
        }
        if c.x2 > maxx {
            maxx = c.x2
        }
        if c.y1 < miny || miny == -1 {
            miny = c.y1
        }
        if c.y2 > maxy {
            maxy = c.y2
        }
        for i := c.x1; i <= c.x2; i++ {
            for j := c.y1; j <= c.y2; j++ {
                space[coord{i, j}] = CLAY
            }
        }
    }

    space[coord{500, 0}] = SPRING

    //render(coord{490,1},coord{510,13}, space, map[coord]bool{})

    fmt.Printf("%d,%d:%d,%d\n", minx, miny, maxx, maxy)

    traceWater(coord{minx,miny}, coord{maxx,maxy}, space)
}
