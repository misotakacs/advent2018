package main

import (
  "flag"
  "fmt"
  "os"
  "strconv"
  "sync"
)

func atoi(s string) int {
    a, err := strconv.Atoi(s)
    if err != nil {
        panic(err)
    }
    return a
}

func power(x, y, sn int) int {
    rID := x + 10
    pw := rID * y
    pw = pw + sn
    pw = pw * rID
    pw = pw % 1000
    pw = pw / 100
    pw = pw - 5
    return pw
}

func populate(n, sn int) [][]int {
    a := make([][]int, n+1)
    for x := 1; x <= n; x++ {
      a[x] = make([]int, n+1)
      for y := 1; y <=n; y++ {
        a[x][y] = power(x, y, sn)
      }
    }
    return a
}

func gsum(n int, g [][]int, sx, sy int) int {
  s := 0
  for x := sx; x < sx + n; x++ {
    for y := sy; y < sy + n; y++ {
      s = s + g[x][y]
    }
  }
  return s
}

func findMax(gn, n int, g [][]int) (int, int, int) {
  max := -1000
  maxX := 1
  maxY := 1
  for x := 1; x <= gn - n + 1; x++ {
    for y := 1; y <= gn - n + 1; y++ {
      psum := gsum(n, g, x, y)
      if max < psum {
        maxX = x
        maxY = y
        max = psum
      }
    }
  }
  return max, maxX, maxY
}

type gpt struct {
  sn int
  x int
  y int
  power int
  n int
}

func tests() {
    // power at position
    t := []gpt{
      {
          sn: 8,
          x: 3,
          y: 5,
          power: 4,
      },
      {
          sn: 57,
          x: 122,
          y: 79,
          power: -5,
      },
      {
          sn: 39,
          x: 217,
          y: 196,
          power: 0,
      },
      {
          sn: 71,
          x: 101,
          y: 153,
          power: 4,
      },
    }
    for _, test := range t {
        fmt.Printf("Testing %d @ %d,%d, expecting %d\n", test.sn, test.x, test.y, test.power)
        g := populate(300, test.sn)
        pwr := g[test.x][test.y]
        if pwr != test.power {
            fmt.Printf("for grid %d, power @ %d,%d should be %d, is %s\n", test.sn, test.x, test.y, test.power, pwr)
            os.Exit(1)
        }
    }

    // total power in 3x3 grid
    tp := []gpt{
        {
            sn: 18,
            x: 33,
            y: 45,
            power: 29,
            n: 3,
        },
        {
            sn: 42,
            x: 21,
            y: 61,
            power: 30,
            n: 3,
        },
        {
            sn: 18,
            x: 90,
            y: 269,
            power: 113,
            n: 16,
        },
    }
    for _, test := range tp {
        g := populate(300, test.sn)
        pwr := gsum(test.n, g, test.x, test.y)
        if pwr != test.power {
            fmt.Printf("for grid %d, total power at %dx%d at %d,%d; expected %d, is %d\n", test.sn, 3, 3, test.x, test.y, test.power, pwr)
            os.Exit(1)
        }
    }
}

type res struct {
    x int
    y int
    n int
    pw int
}

func main() {

    tests()

    flag.Parse()
    args := flag.Args()
    if len(args) != 1 {
        fmt.Printf("Please specify the grid serial number.\n")
        os.Exit(1)
    }

    sn := atoi(args[0])
    g := populate(300, sn)

    tx := 122
    ty := 79

    fmt.Printf("p=%d\n", g[tx][ty])

    m, x, y := findMax(300, 3, g)

    fmt.Printf("%d @ %d,%d\n", m, x, y)

    c := make(chan res)
    f := make(chan bool)

    go func() {
        mr := res{}
        mr.pw = -1000
        num := 0
        for {
            select {
                case r := <-c:
                    if r.pw > mr.pw {
                      mr = r
                    }
                    num++
                    fmt.Printf("%d done, %d/300\n", r.n, num)
                case <-f:
                    fmt.Printf("%d @ %d,%d,%d\n", mr.pw, mr.x, mr.y, mr.n)
                   break
            }
        }
    }()

    var wg sync.WaitGroup   

    for n := 2; n <= 300; n++ {
        wg.Add(1)
        go func(n int) { 
            m, x, y := findMax(300, n, g)
            c <- res{
                pw: m,
                n: n,
                x: x,
                y: y,
            }
            wg.Done()
        }(n)
    }
    wg.Wait()
    f<-true

}

