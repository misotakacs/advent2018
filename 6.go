package main

import (
"flag"
  "fmt"
  "os"
  "bufio"
  "regexp"
  "strconv"
)

func readLines() []string {
    scanner := bufio.NewScanner(os.Stdin)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        panic(err)
    }

    return lines
}

func atoi(s string) int {
    a, err := strconv.Atoi(s)
    if err != nil {
        panic(err)
    }
    return a
}

type coord struct {
    x int
    y int
}

type distcoord struct {
    c coord
    dist int
}

const (
  EMPTY = -1
  EDGE = -2
)

type mp struct {
    // hash of coordinates to ID
    // ID = -1 is empty
    // ID = -2 is edge
    data map[int]int
}

func (m *mp) get(x, y int) int {
    v, ok := m.data[hash(x, y)]
    if !ok {
        return EMPTY
    } else {
        return v
    }
}

func (m *mp) put(x, y, v int) {
    m.data[hash(x, y)] = v
}

func hash(x, y int) int {
    return (x * 1000) + y
}

type origin struct {
    id int
    start coord
    curdist int
    queue []distcoord
    area int
    done bool
}

func abs(a int) int {
    if a < 0 {
        return -a
    } else {
        return a
    }
}

func dist(x1, y1, x2, y2 int) int {
    return abs(x1 - x2) + abs(y1 - y2)
}

func (o *origin) dist(c coord) int {
    return dist(c.x, c.y, o.start.x, o.start.y)
}
func (o *origin) log(f string, args ...interface{}) {
    if o.id == 34 {
        //fmt.Printf(f, args...)
    }
}

func (o *origin) step(m *mp, others map[int]*origin) int {
  if o.done {
      return -1
  }
  if len(o.queue) == 0 {
      o.done = true
      return o.area
  }
  num_skipped := 0
  for len(o.queue) > 0 {
      // Stop if we reach the next level.
      if o.queue[0].dist > o.curdist {
          o.curdist++
          break
      }
      curr := o.queue[0]
      o.queue = o.queue[1:]
      v := m.get(curr.c.x, curr.c.y)
      o.log("%d, %d = %d\n", curr.c.x, curr.c.y, v)
      if v == EMPTY {
          m.put(curr.c.x, curr.c.y, o.id)
          o.area++
      } else if v == EDGE {
          num_skipped++
          continue
      } else if v == o.id {
          num_skipped++
          continue
      } else if v >= 0 {
          // We found someone else's stake.
          // Determine who gets it.
          n, ok  := others[v]
          if !ok {
              panic(fmt.Sprintf("Cannot find origin with id %d", v))
          }
          if o.dist(curr.c) < n.dist(curr.c) {
              n.area--
              m.put(curr.c.x, curr.c.y, o.id)
          } else if o.dist(curr.c) == n.dist(curr.c) {
              m.put(curr.c.x, curr.c.y, EDGE)
              n.area--
              continue
          } else {
              continue
          }
      }
      // We own the field, add surrounding fields.
      // Visited detection is done above.
      d := curr.dist + 1
      o.queue = append(o.queue,
        makedc(curr.c.x - 1, curr.c.y, d),
        makedc(curr.c.x + 1, curr.c.y, d),
        makedc(curr.c.x, curr.c.y - 1, d),
        makedc(curr.c.x, curr.c.y + 1, d))
  }
  o.log("skipped: %d\n", num_skipped)
  o.log("%d: %v\n", len(o.queue), o.queue)
  return -1
}

func makedc(x, y, d int) distcoord {
    return distcoord {
        c: coord {
            x: x,
            y: y,
        },
        dist: d,
    }
}

var num_steps= flag.Int("num_steps", 5, "BFS steps to make")

func main() {
    flag.Parse()
    lines := readLines()
    r := regexp.MustCompile(`(\d+), (\d+)`)
    origins := make(map[int]*origin)
    m := &mp{
        data: make(map[int]int),
    }
    for ix, l := range lines {
        m := r.FindStringSubmatch(l)
        if m == nil {
            panic(fmt.Sprintf("Could not parse %s", l))
        }
        c := coord{
            x: atoi(m[1]),
            y: atoi(m[2]),
        }
        origins[ix] = &origin{
            id: ix,
            start: c,
            queue: []distcoord{{
                c: c,
                dist: 0,
            },},
            area: 1,
        }
    }

    limit := *num_steps
    max_area := 0
    for i := 0; i < limit; i++ {
        for _, o := range origins {
            area := o.step(m, origins)
            if area > 0 { 
              fmt.Printf("%d done\n", o.id)
              if area > max_area {
                  max_area = area
                  fmt.Printf("new max area: %d\n", max_area)
              }
            }
        }
        /*for _, o := range origins {
            fmt.Printf("%d: %d, q=%d\n", o.id, o.area, len(o.queue))
        }*/
        //fmt.Printf("end of step [%d] -------------\n", i)
    }
}
