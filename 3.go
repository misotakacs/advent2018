package main

import (
  "fmt"
  "os"
  "bufio"
  "regexp"
  "strconv"
)

func readLines() []string {
    scanner := bufio.NewScanner(os.Stdin)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        panic(err)
    }

    return lines
}

type rect struct {
    id int
    x1 int
    y1 int
    x2 int
    y2 int
}

func atoi(s string) int {
    a, err := strconv.Atoi(s)
    if err != nil {
        panic(err)
    }
    return a
}

/*
* 
*/

func overlap(r1, r2 rect) *rect {
    or := rect{-1,-1,-1,-1,-1}
    if  r1.x1 >= r2.x1 {
        r1, r2 = r2, r1
    }
    if r1.x2 >= r2.x2 {
        or.x1 = r2.x1
        or.x2 = r2.x2
    } else if r1.x2 >= r2.x1 {
        or.x1 = r2.x1
        or.x2 = r1.x2
    }
    if r1.y1 >= r2.y1 {
      r1, r2 = r2, r1
    }
    if r1.y2 >= r2.y2 {
        or.y1 = r2.y1
        or.y2 = r2.y2
    } else if r1.y2 >= r2.y1 {
        or.y1 = r2.y1
        or.y2 = r1.y2
    }
    if or.x1 != -1 && or.x2 != -1 && or.y1 != -1 && or.y2 != -1 {
        return &or
    }
    return nil
}

func (r *rect) area() int {
    return (r.x2 - r.x1 + 1) * (r.y2 - r.y1 + 1)
}

func (r *rect) overlap(r2 rect) *rect {
    return overlap(*r, r2)
}

func (r *rect) unmarkedArea(d map[int]bool) int {
    area := 0
    for i := r.x1; i <= r.x2; i++ {
        for j := r.y1; j <= r.y2; j++ {
            if !d[hash(i, j)] {
                area++
            }
        }
    }
    return area
}

func (r *rect) mark(d map[int]bool) {
    for i := r.x1; i <= r.x2; i++ {
        for j := r.y1; j <= r.y2; j++ {
            d[hash(i, j)] = true
        }
    }
}

func hash(x, y int) int {
    return 10000 * x + y
}


func main() {
    lines := readLines()

    r := regexp.MustCompile(`#(\d+) @ (\d+),(\d+): (\d+)x(\d+)`)
    fl := []rect{}
    for _, l := range lines {
        m := r.FindStringSubmatch(l)
        if m == nil {
            panic(fmt.Sprintf("Failed to parse: %s", l))
        }
        fl = append(fl, rect{
            id: atoi(m[1]),
            x1: atoi(m[2]),
            y1: atoi(m[3]),
            x2: atoi(m[2]) + atoi(m[4]) - 1,
            y2: atoi(m[3]) + atoi(m[5]) - 1,
        })
    }

    sqIn := 0

    overlaps := []*rect{}
    oIndex := make(map[int]bool)

    for i, r1 := range fl {
        for j := i + 1; j < len(fl); j++ {
            r2 := fl[j]
            if o := overlap(r1, r2); o != nil {
                oIndex[r1.id] = true
                oIndex[r2.id] = true
                sq := o.area()
                discounts := make(map[int]bool)
                for _, prev := range overlaps {
                    if po := prev.overlap(*o); po != nil {
                        sq -= po.unmarkedArea(discounts)
                        po.mark(discounts)
                    }
                }
                overlaps = append(overlaps, o)
                if sq > 0 {
                     sqIn += sq
                }
            }
        }
    }

    for _, r := range fl {
        if !oIndex[r.id] {
            fmt.Printf("No overlap found for %d\n", r.id)
        }
    }

    fmt.Printf("total: %d\n", sqIn)
}
