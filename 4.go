package main

import (
  "fmt"
  "os"
  "bufio"
  "regexp"
  "strconv"
)

func readLines() []string {
    scanner := bufio.NewScanner(os.Stdin)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        panic(err)
    }

    return lines
}

func atoi(s string) int {
    a, err := strconv.Atoi(s)
    if err != nil {
        panic(err)
    }
    return a
}

type nap struct {
    start int
    end int
}

type guard struct {
    id int
    asleep []nap
}

func (g *guard) total() int {
    t := 0
    for _, a := range g.asleep {
        t += a.end - a.start + 1
    }
    return t
}

func (g *guard) maxMin() (int, int) {
    mm := -1
    mmCnt := -1
    for i := 0; i < 60; i++ {
        cnt := 0
        for _, n := range g.asleep {
            if n.start <= i && n.end >= i {
                cnt++
            }
        }
        if cnt > mmCnt {
            mm = i
            mmCnt = cnt
        }
    }
    return mm, mmCnt
}

func main() {
    lines := readLines()
    tr := regexp.MustCompile(`\[\d+-\d+-\d+ (\d+):(\d+)\]`)
    gr := regexp.MustCompile(`Guard #(\d+) begins shift`)
    fa := regexp.MustCompile(`falls asleep`)
    wa := regexp.MustCompile(`wakes up`)

    two := func(r *regexp.Regexp, s string) (int, int, bool) {
        m := r.FindStringSubmatch(s)
        if m == nil {
            return 0, 0, false
        } else {
            return atoi(m[1]), atoi(m[2]), true
        }
    }

    one := func(r *regexp.Regexp, s string) (int, bool) {
        m := r.FindStringSubmatch(s)
        if m == nil {
            return 0, false
        } else {
            return atoi(m[1]), true
        }
    }


    var current *guard
    var maxGuard *guard
    gs := make(map[int]*guard)
    asleepFrom := -1
    for _, l := range lines {
        if id, ok := one(gr, l); ok {
            if g, ok := gs[id]; ok {
                current = g
            } else {
                current = &guard{
                    id: id,
                }
                gs[id] = current
            }
            continue
        }
        if fa.MatchString(l) {
            if _, min, ok := two(tr, l); ok {
                asleepFrom = min
                continue
            } else {
                panic(fmt.Sprintf("Time not recognized: %s", l))
            }
        }
        if wa.MatchString(l) {
            if _, min, ok := two(tr, l); ok {
                if asleepFrom == -1 {
                    panic(fmt.Sprintf("Not asleep, but woken up: %s", l))
                }
                current.asleep = append(current.asleep, nap{asleepFrom, min})
                if maxGuard == nil {
                    maxGuard = current
                } else if current.total() > maxGuard.total() {
                    maxGuard = current
                }
            } else {
                panic(fmt.Sprintf("Time not recognized: %s", l))
            }
        }
    }

    maxMin, _ := maxGuard.maxMin()
    fmt.Printf("Max guard: %d, max min: %d\n", maxGuard.id, maxMin)

    maxGuard = nil
    maxCnt := -1
    for _, g := range gs {
        _, cnt := g.maxMin()
        if maxCnt < cnt {
            maxGuard = g
            maxCnt = cnt
        }
    }
    maxMin, _ = maxGuard.maxMin()
    fmt.Printf("Most frequent minute. Guard %d, min %d\n", maxGuard.id, maxMin)
}
