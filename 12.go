package main

import (
  "fmt"
  "os"
  "bufio"
  "strings"
)

func readLines() []string {
    scanner := bufio.NewScanner(os.Stdin)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        panic(err)
    }

    return lines
}

type Res byte

const (
    Plant Res = '#'
    NoPlant Res = '.'
)

type Pot struct {
    content Res
    next *Pot
}

type Pots struct {
    start *Pot
    end *Pot
}

func advance(hints map[string]Res, state string) string {
    ng := []rune{}
    for ix := 0; ix < len(state) - 4; ix++ {
        sub := state[ix:ix+5]
        ng = append([]rune(ng), rune(hints[sub]))
    }
    return string(ng)
}

func augment(start int, state string) (int, string) {
    ix := strings.IndexRune(state, '#')
    if ix == -1 {
        return start, state
    }
    if ix < 5 {
        state = strings.Repeat(".", 5 - ix) + state
        start = start - (5 - ix)
    }
    ix = strings.LastIndex(state, "#")
    if len(state) - ix < 6 {
        state = state + strings.Repeat(".", 6 - (len(state) - ix))
    }
    return start, state
}

func tests() {
    type t struct {
        in string
        out string
        es int
    }
    tests := []t{
        {"..###..", ".....###.....", -3},
        {"....##.", ".....##.....", -1},
        {"..........", "..........", 0},
        {".....##..", ".....##.....", 0},
    }
    for _, t := range tests {
        start, a := augment(0, t.in)
        if a != t.out {
          fmt.Printf("expected %s => %s, got %s\n", t.in, t.out, a)
          os.Exit(1)
        }
        if start != t.es {
            fmt.Printf("expected %s => %s, start = %d, got %d\n", t.es, start)
            os.Exit(1)
        }
    }
    hints := map[string]Res{
        "...#.": Plant,
        "..#.#": Plant,
        ".#.##": NoPlant,
        "#.###": NoPlant,
        ".....": NoPlant,
        ".###.": Plant,
        "###..": NoPlant,
        "##..#": Plant,
        "#..#.": Plant,
        ".#...": Plant,
        "#....": NoPlant,
        "..#..": NoPlant,
    }
    tests = []t{
        {"...#.###..#.....", "##..#.##.#..", 0},
    }
    for _, t := range tests {
        a := advance(hints, t.in)
        if a != t.out {
          fmt.Printf("expected %s => '%s', got '%s'\n", t.in, t.out, a)
          os.Exit(1)
        }
    }
}

func eval(start int, input string) int {
    sum := 0

    for _, c := range input {
        if c == '#' {
            sum += start
        }
        start++
    }
    return sum
}


func main() {
    l := readLines()

    tests()

   if len(l) != 2 + 32 {
     fmt.Printf("Expected input has 34 lines.\n")
     os.Exit(1)
   }

    prefix := "initial state: "

    input := strings.TrimPrefix(l[0], prefix)
    if len(input) >= len(l[0]) {
        fmt.Printf("Incorrect prefix: %s - not found in %s\n", prefix, l[0])
        os.Exit(1)
    }

    hints := make(map[string]Res)

    for li := 2; li < len(l); li++ {
        parts := strings.Split(l[li], " => ")
        hints[parts[0]] = Res(parts[1][0])
    }

//    fmt.Printf("%v\n", hints)

    start := 0
    last := 0

    for i := 0; i < 100010; i++ {
        start, input = augment(start, input)
        input = advance(hints, input)
        start = start + 2
        e := eval(start, input)
        delta := e - last
        last = e

        if i == 19 || i > 100000 && i < 100010 {
            fmt.Printf("%d: [%d] %d, len=%d, delta=%d, %s\n", i, start, e, len(input), delta, input)
        }
    }

    // "Cycle" discovered - after some state, the pattern stays the same,
    // just moves to the right, increasing value by 62 each generation.
    // Calculated by finding the per-gen delta, sampling a value (at 100,010) and then
    // calculating $value + (50e9 - 100010) * 62.
}

