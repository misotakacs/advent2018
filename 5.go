package main

import (
  "fmt"
  "os"
  "bufio"
//  "regexp"
//  "strconv"
  "strings"
)

func readLines() []string {
    scanner := bufio.NewScanner(os.Stdin)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        panic(err)
    }

    return lines
}

func abs(a int) int {
    if a < 0 {
        return -a
    } else {
        return a
    }
}

func perform(s string, banned byte) string {
    changed := true
    bs := []byte(s)
    for changed {
        changed = false
        dst := 0
        for i := 0; i < len(bs); i++ {
            if bs[i] == banned || bs[i] == banned+32 {
                changed = true
                continue
            }
            if i == len(bs) - 1 || abs(int(bs[i]) - int(bs[i+1])) != 32 {
                bs[dst] = bs[i]
                dst++
            } else {
                changed = true
                i += 1
                continue
            }
        }
        bs = bs[:dst]
    }

    last := byte(0)
    for _, b := range bs {
        if abs(int(last) - int(b)) == 32 {
            fmt.Printf("found elmininable chars: %c%c\n", last, b)
        }
        last = b
        if b == banned || b == banned+32 {
            fmt.Printf("found banned char %d\n", banned)
        }
    }

    return string(bs)
}

func main() {
    lines := readLines()
    line := strings.TrimSpace(lines[0])

    minRes := 1000000
    for i := byte(65); i <= 90; i++ {
        res := perform(line, i)
        if minRes > len(res) {
            minRes = len(res)
        }
    }
    fmt.Printf("minlen = %d\n", minRes)
}
