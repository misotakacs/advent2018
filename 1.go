package main

import (
  "fmt"
  "os"
  "bufio"
  "strconv"
)

func readLines() []string {
    scanner := bufio.NewScanner(os.Stdin)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        panic(err)
    }

    return lines
}

func main() {
  lines := readLines()
  sum := 0
  sums := make(map[int]bool)
  found := false
  for !found {
      for _, l := range lines {
          a, err := strconv.Atoi(l)
          if err != nil {
              panic(err)
          }
          sum += a
          if sums[sum] {
              fmt.Printf("Sum found: %d\n", sum)
              found = true
              break
          } else {
              sums[sum] = true
          }
      }
  }
  fmt.Printf("Final sum: %d\n", sum)
}
