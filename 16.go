package main

import (
  "fmt"
  "os"
  "bufio"
  "regexp"
  "strconv"
)

func readLines() []string {
    scanner := bufio.NewScanner(os.Stdin)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        panic(err)
    }

    return lines
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

type opcode struct {
    name string
    fn oper
}

type instr struct {
  opcode int
  A int
  B int
  C int  
}

type op func(int, int) int
type oper func(r []int, i instr)

func add(i1, i2 int) int {
    return i1+i2
}

func mul(i1, i2 int) int {
    return i1*i2
}

func and(i1, i2 int) int {
    return i1 & i2
}

func or(i1, i2 int) int {
    return i1 | i2
}

func set(i1, i2 int) int {
    return i1
}

func gt(i1, i2 int) int {
    if i1 > i2 {
        return 1
    } else {
        return 0
    }
}

func eq(i1, i2 int) int {
    if i1 == i2 {
        return 1
    } else {
        return 0
    }
}

func opir(f op) oper {
    return func(r []int, i instr) {
//        fmt.Printf("opir; %v, %v\n", r, i)
        r[i.C] = f(i.A, r[i.B])
    }
}

func opr(f op) oper {
    return func(r []int, i instr) {
//      fmt.Printf("opr; %v, %v\n", r, i)
      r[i.C] = f(r[i.A], r[i.B])
    }
}

func opi(f op) oper {
  return func(r []int, i instr) {
//      fmt.Printf("opi; %v, %v\n", r, i)
      r[i.C] = f(r[i.A], i.B)
  }
}

func eqa(r1, r2 []int) bool {
    if len(r1) != len(r2) {
        return false
    }
    for i := 0; i < len(r1); i++ {
        if r1[i] != r2[i] {
            return false
        }
    }
    return true
}

var allOps = []opcode{
    {
        name: "addr",
        fn: opr(add),
    },
    {
        name: "addi",
        fn: opi(add),
    },
    {
        name: "mulr",
        fn: opr(mul),
    },
    {
        name: "muli",
        fn: opi(mul),
    },
    {
        name: "banr",
        fn: opr(and),
    },
    {
        name: "bani",
        fn: opi(and),
    },
    {
        name: "borr",
        fn: opr(or),
    },
    {
        name: "bori",
        fn: opi(or),
    },
    {
        name: "setr",
        fn: opr(set),
    },
    {
        name: "seti",
        fn: opir(set),
    },
    {
        name: "gtir",
        fn: opir(gt),
    },
    {
        name: "gtri",
        fn: opi(gt),
    },
    {
        name: "gtrr",
        fn: opr(gt),
    },
    {
        name: "eqir",
        fn: opir(eq),
    },
    {
        name: "eqri",
        fn: opi(eq),
    },
    {
        name: "eqrr",
        fn: opr(eq),
    },
}

var debugged = 3

func process(r1, r2 []int, i instr, allOps []opcode, opmap map[int]map[string]int) int {
    cnt := 0
    if _, ok := opmap[i.opcode]; !ok {
        opmap[i.opcode] = make(map[string]int)
    }
    if i.opcode == debugged {
        fmt.Printf("R: %v -> %v\n", r1, r2)
        fmt.Printf("I: %v\n", i)
    }
    for _, o := range allOps {
        r := make([]int, 4)
        copy(r, r1)
        o.fn(r, i)
        if r2[i.C] == r[i.C] {
            if i.opcode == debugged {
                fmt.Printf("%s matches\n", o.name)
            }
            if v := opmap[i.opcode][o.name]; v >= 0 {
                opmap[i.opcode][o.name] = v + 1
            }
            cnt++
        } else {
            opmap[i.opcode][o.name] = -1
        }
    }
    return cnt
}

func main() {
    lines := readLines()
    bef := regexp.MustCompile(`Before:\s+\[(\d+), (\d+), (\d+), (\d+)\]`)
    aft := regexp.MustCompile(`After:\s+\[(\d+), (\d+), (\d+), (\d+)\]`)
    instrex := regexp.MustCompile(`^(\d+) (\d+) (\d+) (\d+)`)

    inside := false
    var beforeRegs,afterRegs []int
    var instruction *instr
    total := 0
    opmap := make(map[int]map[string]int)
    for _, l := range lines {
        if !inside && instrex.MatchString(l) {
            fmt.Printf("Done parsing.\n")
            break
        }
        if !inside && aft.MatchString(l) {
            panic(fmt.Sprintf("Found After, but not Before: %s", l))
        }
        if !inside {
            m := bef.FindStringSubmatch(l)
            if m != nil {
                beforeRegs = []int{atoi(m[1]), atoi(m[2]), atoi(m[3]), atoi(m[4])}
                instruction = nil
                inside = true
                continue
            }
        } else {
            m := aft.FindStringSubmatch(l)
            if m != nil {
                if instruction == nil {
                    panic(fmt.Sprintf("Found After, but don't have instruction: %s", l))
                }
                afterRegs = []int{atoi(m[1]), atoi(m[2]), atoi(m[3]), atoi(m[4])}
                inside = false
                cnt := process(beforeRegs, afterRegs, *instruction, allOps, opmap)
                if cnt >= 3 {
                    total++
                }
                continue
            }
            m = instrex.FindStringSubmatch(l)
            if m != nil {
                instruction = &instr{
                    opcode: atoi(m[1]),
                    A: atoi(m[2]),
                    B: atoi(m[3]),
                    C: atoi(m[4]),
                }
                continue
            }
        }
        fmt.Printf("%v\n", opmap[debugged])
//        fmt.Printf("Unrecognized line: %s\n", l)
    }
    ins := make(map[string][]int)
    fmt.Printf("Total = %d\n", total)
    for opcode, m := range opmap {
        for i, v := range m {
          if v > 0 {
            fmt.Printf("%d: %s:%d\n", opcode, i, v)
            ins[i] = append(ins[i], opcode)
          }
        }
    }

    fmt.Printf("%v\n", ins)

    remove := func(a []int, e int) []int {
        ret := []int{}
        for _, el := range a {
            if el != e {
                ret = append(ret, el)
            }
        }
        return ret
    }

    eliminated := make(map[int]bool)
    for {
        el := -1
        for _, opcodes  := range ins {
            if len(opcodes) == 1 && !eliminated[opcodes[0]] {
                el = opcodes[0]
                break
            }
        }
        if el == -1 {
            break
        }
        for i, opcodes := range ins {
            if len(opcodes) > 1 {
                ins[i] = remove(ins[i], el)
            }
        }
        eliminated[el] = true
        fmt.Printf("rem=%d, %v\n", el, ins)
    }
    fmt.Printf("%v\n", ins)

    code := []*instr{}
    after := false
    for _, l := range lines {
        if l == "" {
            continue
        }
        if m := instrex.FindStringSubmatch(l); m != nil && after {
            code = append(code, &instr{
                    opcode: atoi(m[1]),
                    A: atoi(m[2]),
                    B: atoi(m[3]),
                    C: atoi(m[4]),
                })
            continue
        }
        if !aft.MatchString(l) {
            after = false
        } else {
            after = true
        }
    }
    allOpsMap := make(map[string]opcode)
    for _, o := range allOps {
        allOpsMap[o.name] = o
    }
    fmt.Printf("%v\n", allOpsMap)
    omap := make(map[int]opcode)
    for name, codes := range ins {
        omap[codes[0]] = allOpsMap[name]
    }
    r := []int{0,0,0,0}
    for _, c := range code {
//        fmt.Printf("%s, %v\n", omap[c.opcode].name, *c)
        omap[c.opcode].fn(r, *c)
    }
    fmt.Printf("regs = %v\n", r)
}

