package main

import (
  "fmt"
  "os"
  "bufio"
  "regexp"
  "strconv"
)

func readLines() []string {
    scanner := bufio.NewScanner(os.Stdin)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        panic(err)
    }

    return lines
}

func atoi(s string) int {
    a, err := strconv.Atoi(s)
    if err != nil {
        panic(err)
    }
    return a
}

type pt struct {
    posx int
    posy int
    vx int
    vy int
}

func min(a, b int) int {
    if a < b {
        return a
    } else {
        return b
    }
}

func max(a, b int) int {
    if a > b {
        return a
    } else {
        return b
    }
}


func show(pts []*pt) bool {
   minx := 1000
   miny := 1000

   maxx := -1000
   maxy := -1000
   
   scr := make(map[int]map[int]bool)
   for _, p := range pts {

      minx = min(p.posx, minx)
      miny = min(p.posy, miny)
      maxx = max(p.posx, maxx)
      maxy = max(p.posy, maxy)

      e := scr[p.posx]
       if e == nil {
           e = make(map[int]bool)
           scr[p.posx] = e
       }
       e[p.posy] = true
    //   fmt.Printf("setting %d,%d\n", p.posx, p.posy)
   }
fmt.Printf("\n%d x %d\n", maxx-minx, maxy-miny)

if maxx - minx > 100 || maxy - miny > 200 {
    return false
}

   x := minx
   y := miny

   line := ""

   for {
       c := "."
       e := scr[x]
       if e != nil {
           if e[y] {
               c = "#"
           }
       }
       line = line + c
       x++
       if x > maxx {
           fmt.Printf("%s\n", line)
           line = ""
           y++
           x = minx
           if y > maxy {
               break
           }
       }
   }

   return true
}

func move(pts []*pt) {
    for _, p := range pts {
        p.posx += p.vx
        p.posy += p.vy
    }
}


func main() {
    lines := readLines()

    points := []*pt{}

    r := regexp.MustCompile(`position=<\s*([\-0-9]+),\s+([\-0-9]+)>\s+velocity=<\s*([\-0-9]+),\s+([\-0-9]+)>`)
    for _, l := range lines {
        m := r.FindStringSubmatch(l)
        if m == nil {
            panic(fmt.Sprintf("Failed to parse: %s", l))
        }
        p := &pt{
            posx: atoi(m[1]),
            posy: atoi(m[2]),
            vx: atoi(m[3]),
            vy: atoi(m[4]),
        }
        points = append(points, p)
    }

    shown := false

    sec := 0

    for i := 0; i < 100000; i++ {
      s := show(points)
      move(points)

      sec++

      fmt.Printf("%d seconds\n", sec)

      if shown && !s {
          break
      }
      shown = s
    }
}
