package main

import (
  "flag"
  "fmt"
  "os"
  "bufio"
  "regexp"
  "sort"
  "strings"
)

func readLines() []string {
    scanner := bufio.NewScanner(os.Stdin)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        panic(err)
    }

    return lines
}

type step struct {
  name string
  deps map[string]bool
}

var (
  num_workers = flag.Int("num_workers", 5, "Total number of workers.")
  base_duration = flag.Int("base_duration", 60, "The base duration of a job.")
)

func main() {
    flag.Parse()
    lines := readLines()
    r := regexp.MustCompile(`Step ([A-Z]) must be finished before step ([A-Z]) can begin`)
    deps := make(map[string][]string)
    steps := make(map[string]*step)
    resolved := make(map[string]bool)
    for _, l := range lines {
        m := r.FindStringSubmatch(l)
        if m == nil {
            panic(fmt.Sprintf("Failed to parse %s", l))
        }
        name := m[2]
        dep := m[1]
        s := steps[name]
        if s == nil {
            s = &step{
                name: name,
                deps: make(map[string]bool),
            }
            steps[name] = s
        }
        s.deps[dep] = true
        if _, ok := steps[dep]; !ok {
            steps[dep] = &step{
                name: dep,
                deps: make(map[string]bool),
            }
        }
        d, ok := deps[dep]
        if ok {
            d = append(d, name)
        } else {
            d = []string{name}
        }
        deps[dep] = d
        resolved[name] = true
        resolved[dep] = true
    }
    for _, steps := range deps {
        for _, s := range steps {
            delete(resolved, s)
        }
    }

    type worker struct {
        processing string
        doneAt int
    }

    time := 0
    workers := []*worker{
        &worker{},
        &worker{},
        &worker{},
        &worker{},
        &worker{},
    }
    workers = workers[0:*num_workers]
    fmt.Printf("Num workers: %d\n", len(workers))
    ready := []string{}
    for d := range resolved {
        ready = append(ready, d)
    }
    sort.Strings(ready)
    finished := false
    processed := 0
    for !finished {
        //allocated := false
        for _, w := range workers {
          if w.doneAt == time && w.processing != "" {
            processed++
            fmt.Printf("[%d] finished %s\n", time, w.processing)
            if len(steps) == processed {
                finished = true
                break
            }
            d := deps[w.processing]
            for _, s := range d {
                delete(steps[s].deps, w.processing)
                if len(steps[s].deps) == 0 {
                    fmt.Printf("Unblocked step: %s\n", s)
                    ins(&ready, 0, s)
                    if !sort.StringsAreSorted(ready) {
                        fmt.Printf("Not sorted: %v\n", ready)
                        os.Exit(-1)
                    }
                }
            }
            w.processing = ""
          }
        }
        // now schedule new work
        for ix, w := range workers {
            if w.processing == "" && len(ready) > 0 {
                w.processing = ready[0]
                w.doneAt = time + dur(ready[0])
                fmt.Printf("[%d] scheduling job %s on worker %d, finishing at %d\n", time, w.processing, ix, w.doneAt)
                ready = ready[1:]
            }
        }
        time++
        fmt.Printf("time: %d\n", time)
    }

    fmt.Printf("%v\n", steps["E"])
}

func dur(s string) int {
    b := []byte(s)
    return int(b[0] - 64 + byte(*base_duration))
}

// insert into sorted array, but only starting at
// certain position
func ins(ss *[]string, ix int, s string) {
    for i := ix; i < len(*ss); i++ {
      if strings.Compare(s, (*ss)[i]) < 0 {
          *ss = append(*ss, "")
          copy((*ss)[i+1:], (*ss)[i:])
          (*ss)[i] = s
          return
      }
    }
    *ss = append(*ss, s)
}
