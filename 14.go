package main

import (
	"fmt"
	"os"
	"strconv"
)

func atoi(s string) int {
	a, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	return a
}

func parse(num int) []int {
	parts := []int{}
	for num > 9 {
		parts = append(parts, num%10)
		num = num / 10
	}
	parts = append(parts, num)
    r := []int{}
	for i := len(parts) - 1; i >= 0; i-- {
		r = append(r, parts[i])
	}
    return r
}

func sum(r, el []int) int {
	s := 0
	for _, e := range el {
		s += r[e]
	}
	return s
}

func dp(r, elves []int) {
	for ix, n := range r {
		if ix == elves[0] {
			fmt.Printf("(%d) ", n)
		} else if ix == elves[1] {
			fmt.Printf("[%d] ", n)
		} else {
			fmt.Printf("%d ", n)
		}
	}
	fmt.Print("\n")
}

func eq(a, b []int) bool {
    if len(a) != len(b) {
        panic("Different lenght!")
    }

    for ix, e := range a {
        if b[ix] != e {
            return false
        }
    }

    return true
}

func main() {
	n := atoi(os.Args[1])
    max := atoi(os.Args[2])

	fmt.Printf("Solving for %d: \n", n)

	r := []int{3, 7}
	elves := []int{0, 1}

    np := parse(n)

	for len(r) <= max {

		num := sum(r, elves)
		r = append(r, parse(num)...)
		for ix, e := range elves {
			elves[ix] = (1 + e + r[e]) % len(r)
		}
        found := false
        if len(r) >= len(np) + 1 {
            if eq(r[len(r) - len(np) - 1:len(r) - 1], np) {
                found = true
                fmt.Printf("first")
            }
            if eq(r[len(r) - len(np):len(r)], np) {
                found = true
                fmt.Printf("second")
            }
            if found {
                fmt.Printf("found! %d\n", len(r))
                break
            }
        }
 //       fmt.Printf("%d\n", len(r))
	}

//    fmt.Printf("%v\n", r[n:n+10])
}
