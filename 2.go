package main

import (
  "fmt"
  "os"
  "bufio"
)

func readLines() []string {
    scanner := bufio.NewScanner(os.Stdin)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        panic(err)
    }

    return lines
}

func main() {
    lines := readLines()
    cnt2 := 0
    cnt3 := 0
    for _, l := range lines {
        cnts := make(map[rune]int)
        for _, c := range l {
            cnts[c]++
        }
        lcnt2, lcnt3 := 0, 0
        for _, cnt := range cnts {
            if cnt == 2 {
                lcnt2 = 1
            }
            if cnt == 3 {
                lcnt3 = 1
            }
        }
        cnt2 += lcnt2
        cnt3 += lcnt3
    }

    fmt.Printf("Checksum: %d * %d = %d\n", cnt2, cnt3, cnt2*cnt3)


    for i1, s1 := range lines {
        for i2, s2 := range lines {
            if i1 == i2 {
                continue
            }
            if len(s1) != len(s2) {
                panic(fmt.Sprintf("Line lengths don't match for %s vs %s.", s1, s2))
            }
            diffIx := -1
            for j := 0; j < len(s1); j++ {
                if s1[j] == s2[j] {
                    continue
                }
                if diffIx != -1 {
                    diffIx = -2
                    break
                }
                diffIx = j
            }
            if diffIx >= 0 {
                fmt.Printf("Found the strings: %s and %s\n", s1, s2)
                fmt.Printf("Common chars are: %s\n", string(append([]rune(s1[:diffIx]), []rune(s1[diffIx + 1:])...)))
                break
            }
        }
    }
}
