package main

import (
  "fmt"
  "flag"
  "os"
  "strconv"
)

type node struct {
    prev *node
    next *node
    value int
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

func walk(n *node, amount int) *node {
    if amount == 0 {
        return n
    } else if amount < 0 {
        return walk(n.prev, amount+1)
    } else {
        return walk(n.next, amount-1)
    }
}

func prt(n *node) {
    start := n
    a := []int{}

    for {
        a = append(a, n.value)
        n = n.next
        if n == start {
            break
        }
    }
    fmt.Printf("%v\n", a)
}

func main() {
    flag.Parse()
    args := flag.Args()

    players := make(map[int]int)

    if len(args) < 2 {
        fmt.Println("Please specify number of players and max bead number.")
        os.Exit(1)
    }

    numPlayers := atoi(args[0])
    maxBeads := atoi(args[1])

    currBead := 1
    currPlayer := 0

    curr := &node{
        value: 0,
    }
    curr.prev = curr
    curr.next = curr

    maxScore := 0

//    start := curr

    for {
        if currBead % 23 == 0 {
            curr = walk(curr, -7)
            players[currPlayer] = players[currPlayer] + currBead + curr.value
            if players[currPlayer] > maxScore {
                maxScore = players[currPlayer]
            }
            curr.prev.next = curr.next
            curr.next.prev = curr.prev
            curr = curr.next
        } else {
            curr = walk(curr, 1)
            next := curr.next
            curr.next = &node{
                prev: curr,
                next: next,
                value: currBead,
            }
            next.prev = curr.next
            curr = curr.next
        }
        currBead++
        currPlayer = (currPlayer + 1) % numPlayers

//        prt(start)

        if currBead == maxBeads {
            break
        }
    }

    fmt.Printf("players = %d, beads = %d, maxScore = %d\n", numPlayers, maxBeads, maxScore)
}
