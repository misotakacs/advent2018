package main

import (
  "fmt"
  "os"
  "bufio"
/*  "regexp"
  "strconv"*/
)

func readLines() []string {
    scanner := bufio.NewScanner(os.Stdin)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        panic(err)
    }

    return lines
}

func sumMeta(a []int, ix int) (int, int, int) {
    c := ix + 2
    sum := 0
    nval := []int{}
    for i := 0; i < a[ix]; i++ {
        var ps, v int
        v, ps, c = sumMeta(a, c)
        nval = append(nval, v)
        sum += ps
    }
    msum := 0
    for i := c; i < c + a[ix+1]; i++ {
        msum += a[i]
    }
    val := 0
    if a[ix] == 0 {
        val = msum
    } else {
        for i := c; i < c + a[ix+1]; i++ {
            if a[i] == 0 {
                continue
            }
            if a[i] > a[ix] {
                continue
            }
            val += nval[a[i]-1]
        }
    }
    return val, sum + msum, c + a[ix+1]
}

func main() {
    all := []int{}
    for {
        var i int
        _, err := fmt.Scanf("%d", &i)
        if err != nil {
            break
        }
        all = append(all, i)
    }
    v, s, _ := sumMeta(all, 0)
    fmt.Printf("v=%d, s=%d\n", v, s)
    
}
